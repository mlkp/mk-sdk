<?php

namespace Mailkeeper\SDK;

use Mailkeeper\SDK\Http\Client;
use Mailkeeper\SDK\Resources\Attributes;
use Mailkeeper\SDK\Resources\Custom;
use Mailkeeper\SDK\Resources\Event;
use Mailkeeper\SDK\Resources\EventSend;
use Mailkeeper\SDK\Resources\Register;
use Mailkeeper\SDK\Resources\RegisterReceiver;
use Mailkeeper\SDK\Resources\TriggerSend;

/**
 * Class Factory
 *
 * @method RegisterReceiver registerReceiver()
 * @method Event event()
 * @method Attributes attributes()
 * @method Register register()
 * @method TriggerSend triggerSend()
 * @method EventSend eventSend()
 * @method Custom custom()
 * @package Mailkeeper\SDK
 */
class Factory
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @param array $config An array of configurations. You need at least the 'api-key'.
     * @param Client $client
     * @param array $clientOptions options to be send with each request
     * @param bool $wrapResponse wrap request response in own Response object
     */
    public function __construct(array $config = [], Client $client = null, array $clientOptions = [], $wrapResponse = true)
    {
        if (is_null($client)) {
            $client = new Client($config, null, $clientOptions, $wrapResponse);
        }
        $this->client = $client;
    }

    /**
     * Return an instance of a Resource based on the method called.
     *
     * @param $name
     * @param $args
     * @return mixed
     */
    public function __call($name, $args)
    {
        $resource = 'Mailkeeper\\SDK\\Resources\\' . ucfirst($name);

        return new $resource($this->client, ...$args);
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Create an instance of the service with an API token.
     *
     * @param string $apiKey Mailkeeper API token
     * @param Client $client an Http client
     * @param array $clientOptions options to be send with each request
     * @param bool $wrapResponse wrap request response in own Response object
     *
     * @return static
     */
    public static function create($apiKey = null, Client $client = null, $clientOptions = [], $wrapResponse = true)
    {
        return new static(['api-key' => $apiKey], $client, $clientOptions, $wrapResponse);
    }
}
