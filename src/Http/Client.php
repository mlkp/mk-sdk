<?php

namespace Mailkeeper\SDK\Http;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Mailkeeper\SDK\Exceptions\BadRequest;
use Mailkeeper\SDK\Exceptions\InvalidArgument;
use Mailkeeper\SDK\Exceptions\MailkeeperException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Client
 * @package Mailkeeper\SDK\Http
 */
class Client
{
    const API_TOKEN_HEADER = 'Api-Token';
    const USER_AGENT_HEADER = 'User-Agent';

    /** @var string */
    public $apiUrl;

    /** @var string */
    public $apiKey;

    /** @var int */
    public $productId;

    /** @var \GuzzleHttp\Client */
    public $client;

    /**
     * Guzzle allows options into its request method. Prepare for some defaults.
     *
     * @var array
     */
    protected $clientOptions = [];

    /**
     * if set to false, no Response object is created, but the one from Guzzle is directly returned
     * comes in handy own error handling.
     *
     * @var bool
     */
    protected $wrapResponse = true;

    /** @var string */
    protected $userAgent = 'Mailkeeper SDK';

    /**
     * @param array $config Configuration array
     * @param GuzzleClient $client The Http Client (Defaults to Guzzle)
     * @param array $clientOptions options to be passed to Guzzle upon each request
     * @param bool $wrapResponse wrap request response in own Response object
     */
    public function __construct($config = [], $client = null, $clientOptions = [], $wrapResponse = true)
    {
        $this->clientOptions = $clientOptions;
        $this->wrapResponse = $wrapResponse;

        $this->apiKey =
            isset($config['api-key']) ? $config['api-key'] : getenv('MAILKEEPER_API_KEY');
        $this->productId =
            isset($config['product-id']) ? $config['product-id'] : getenv('MAILKEEPER_PRODUCT_ID');
        $this->apiUrl =
            isset($config['api-url']) ? $config['api-url'] : getenv('MAILKEEPER_API_URL');

        if (!$this->apiUrl) {
            throw new InvalidArgument('API URL is required');
        }

        if (!$this->productId || !$this->apiKey) {
            throw new InvalidArgument('Product ID and API key is required');
        }

        if (is_null($client)) {
            $client = new GuzzleClient();
        }
        $this->client = $client;
    }

    /**
     * Send the request...
     *
     * @param string $method The HTTP request verb
     * @param string $endpoint The Mailkeeper API endpoint
     * @param array $options An array of options to send with the request
     * @param string $query_string A query string to send with the request
     * @return Response|ResponseInterface
     * @throws MailkeeperException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($method, $endpoint, array $options = [], $query_string = null)
    {
        if (empty($this->apiKey)) {
            throw new InvalidArgument('You must provide a Mailkeeper api key.');
        }

        $url = $this->generateUrl($endpoint, $query_string);

        $options = array_merge($this->clientOptions, $options);
        $options['headers'][self::USER_AGENT_HEADER] = $this->userAgent;
        $options['headers'][self::API_TOKEN_HEADER] = $this->apiKey;

        try {
            if (false === $this->wrapResponse) {
                return $this->client->request($method, $url, $options);
            }

            return new Response($this->client->request($method, $url, $options));
        } catch (ServerException $e) {
            throw MailkeeperException::create($e);
        } catch (ClientException $e) {
            throw BadRequest::create($e);
        }
    }

    /**
     * Generate the full endpoint url, including query string.
     *
     * @param string $endpoint the Mailkeeper API endpoint
     * @param string $queryString the query string to send to the endpoint
     *
     * @return string
     */
    protected function generateUrl($endpoint, $queryString = null)
    {
        $url = $this->apiUrl . $endpoint . '?';

        $queryParams = [];

        if ($this->productId) {
            $queryParams['product_id'] = $this->productId;
        }

        $queryString .= $this->addQuery($queryString, http_build_query($queryParams));

        return $url . $queryString;
    }

    /**
     * @param string $queryString the query string to send to the endpoint
     * @param string $addition addition query string to send to the endpoint
     *
     * @return string
     */
    protected function addQuery($queryString, $addition)
    {
        $result = '';

        if (!empty($addition)) {
            if (empty($queryString)) {
                $result = $addition;
            } else {
                $result .= '&' . $addition;
            }
        }

        return $result;
    }
}
