<?php

if (!function_exists('build_query_string')) {
    /**
     * Generate a query string.
     *
     * @param array $params
     *
     * @return string
     */
    function build_query_string($params = [])
    {
        if (empty($params)) {
            return '';
        }

        $query = '';
        foreach ($params as $key => $value) {
            if (is_array($value)) {
                $query .= build_batch_query_string($key, $value);

                continue;
            }

            $parameter = urlencode($key);
            $propertyValue = is_bool($value) ? 'true' : urlencode($value);
            $query .= "&{$parameter}={$propertyValue}";
        }

        return $query ?: '';
    }
}

if (!function_exists('build_batch_query_string')) {
    /**
     * Generate a query string for batch requests.
     *
     * @param string $key the name of the query variable
     * @param array $items an array of item values for the variable
     *
     * @return string
     */
    function build_batch_query_string($key, $items)
    {
        return array_reduce($items, function ($query, $item) use ($key) {
            return $query . '&' . urlencode($key) . '=' . urlencode($item);
        }, '');
    }
}
