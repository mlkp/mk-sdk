<?php

namespace Mailkeeper\SDK\Resources;

use Mailkeeper\SDK\Http\Response;

/**
 * Class Custom
 * @package Mailkeeper\SDK\Resources
 */
class Custom extends Resource
{
    /**
     * @param $method
     * @param $endpoint
     * @param $params
     * @return Response|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Mailkeeper\SDK\Exceptions\MailkeeperException
     */
    public function request($method, $endpoint, $params)
    {
        $extraParams = [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ];

        $queryString = null;

        if (strtolower($method) === 'get') {
            $queryString = build_query_string($params);
        }
        if (strtolower($method) !== 'get') {
            $extraParams['body'] = json_encode($params);
        }

        return $this->client->request($method, $endpoint, $extraParams, $queryString);
    }
}
