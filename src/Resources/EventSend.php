<?php

namespace Mailkeeper\SDK\Resources;

use Mailkeeper\SDK\Http\Response;

/**
 * Class EventSend
 * @package Mailkeeper\SDK\Resources
 */
class EventSend extends Resource
{
    /**
     * @param array $params
     * @return Response|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Mailkeeper\SDK\Exceptions\MailkeeperException
     */
    public function send($params)
    {
        $endpoint = '/api/v1/event-send';

        return $this->client->request(
            'post',
            $endpoint,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'body' => json_encode($params),
            ]
        );
    }
}
