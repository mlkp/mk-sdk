<?php

namespace Mailkeeper\SDK\Resources;

use Mailkeeper\SDK\Http\Client;

/**
 * Class Resource
 * @package Mailkeeper\SDK\Resources
 */
abstract class Resource
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * Makin' a resource.
     *
     * @param Client $client
     */
    public function __construct($client)
    {
        $this->client = $client;
    }
}
