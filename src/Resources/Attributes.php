<?php

namespace Mailkeeper\SDK\Resources;

use Mailkeeper\SDK\Http\Response;

/**
 * Class Attributes
 * @package Mailkeeper\SDK\Resources
 */
class Attributes extends Resource
{
    /**
     * @param $params
     * @return Response|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Mailkeeper\SDK\Exceptions\MailkeeperException
     */
    public function update($params)
    {
        $endpoint = '/api/v1/update-attributes';

        return $this->client->request(
            'patch',
            $endpoint,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'body' => json_encode($params),
            ]
        );
    }
}
