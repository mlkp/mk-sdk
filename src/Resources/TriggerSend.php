<?php

namespace Mailkeeper\SDK\Resources;

use Mailkeeper\SDK\Http\Response;

/**
 * Class TriggerSend
 * @package Mailkeeper\SDK\Resources
 * @deprecated deprecated since version 2.0.1
 */
class TriggerSend extends Resource
{
    /**
     * @param array $params
     * @return Response|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Mailkeeper\SDK\Exceptions\MailkeeperException
     * @deprecated deprecated since version 2.0.1
     */
    public function send($params)
    {
        $endpoint = '/api/v1/trigger-send';

        return $this->client->request(
            'post',
            $endpoint,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'body' => json_encode($params),
            ]
        );
    }
}
