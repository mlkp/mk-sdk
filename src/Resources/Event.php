<?php

namespace Mailkeeper\SDK\Resources;

use Mailkeeper\SDK\Http\Response;

/**
 * Class Event
 * @package Mailkeeper\SDK\Resources
 */
class Event extends Resource
{
    /**
     * @param $params
     * @return Response|\Psr\Http\Message\ResponseInterface
     * @throws \Mailkeeper\SDK\Exceptions\MailkeeperException
     */
    public function confirm($params)
    {
        $endpoint = '/api/v1/confirm';

        return $this->client->request('patch', $endpoint, [], build_query_string($params));
    }

    /**
     * @param $params
     * @return Response|\Psr\Http\Message\ResponseInterface
     * @throws \Mailkeeper\SDK\Exceptions\MailkeeperException
     */
    public function unsubscribe($params)
    {
        $endpoint = '/api/v1/unsubscribe';

        return $this->client->request(
            'patch',
            $endpoint,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'body' => json_encode($params),
            ]
        );
    }
}
