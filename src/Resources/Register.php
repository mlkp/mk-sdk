<?php

namespace Mailkeeper\SDK\Resources;

use Mailkeeper\SDK\Http\Response;

/**
 * Class Register
 * @package Mailkeeper\SDK\Resources
 */
class Register extends Resource
{
    /**
     * @param $params
     * @return Response|\Psr\Http\Message\ResponseInterface
     * @throws \Mailkeeper\SDK\Exceptions\MailkeeperException
     */
    public function register($params)
    {
        $endpoint = '/api/v1/registration';

        return $this->client->request(
            'post',
            $endpoint,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'body' => json_encode($params),
            ]
        );
    }
}
