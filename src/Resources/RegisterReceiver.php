<?php

namespace Mailkeeper\SDK\Resources;

use Mailkeeper\SDK\Http\Response;

/**
 * Class RegisterReceiver
 * @package Mailkeeper\SDK\Resources
 */
class RegisterReceiver extends Resource
{
    /**
     * @param array $params
     * @return Response|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Mailkeeper\SDK\Exceptions\MailkeeperException
     */
    public function register($params)
    {
        $endpoint = '/api/v1/registration/receiver';

        return $this->client->request(
            'post',
            $endpoint,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'body' => json_encode($params),
            ]
        );
    }
}
