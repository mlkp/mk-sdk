<?php

namespace Mailkeeper\SDK\Exceptions;

/**
 * Class InvalidArgument
 * @package Mailkeeper\SDK\Exceptions
 */
class InvalidArgument extends \InvalidArgumentException
{
}
