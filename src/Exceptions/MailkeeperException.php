<?php

namespace Mailkeeper\SDK\Exceptions;

use Exception;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;

/**
 * Class MailkeeperException
 * @package Mailkeeper\SDK\Exceptions
 */
class MailkeeperException extends Exception
{
    /** @var null|Response */
    protected $response;

    /**
     * @return null|Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param RequestException $guzzleException
     * @return static
     */
    public static function create(RequestException $guzzleException): self
    {
        $e = new static($guzzleException->getMessage(), $guzzleException->getCode());

        $e->response = $guzzleException->getResponse();

        return $e;
    }
}
