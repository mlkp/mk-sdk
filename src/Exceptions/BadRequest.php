<?php

namespace Mailkeeper\SDK\Exceptions;

/**
 * Class BadRequest
 * @package Mailkeeper\SDK\Exceptions
 */
class BadRequest extends MailkeeperException
{
}
