# Installation

Add following code to your ```composer.json``` file
```json
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/mlkp/mk-sdk"
    }
],
"require": {
    "mlkp/mk-sdk": "^2.0",
},
```

Execute command
```bash
$ composer require mlkp/mk-sdk
```
OR for update
```bash
$ composer update mlkp/mk-sdk
```
## Configuration

Add this variables to ```.env```
```
MAILKEEPER_API_URL=https://some.url
MAILKEEPER_PRODUCT_ID=
MAILKEEPER_API_KEY=
```

⚠️ Don't pass `product-id`, `api-url` and `api-key` to the client if you use .env variables ⚠️



### Examples Using Factory
```php
$mailkeeper = Mailkeeper\SDK\Factory::create();
```

OR instantiate by passing a configuration array.
Required values are `api-key`, `api-url` and `product-id`

```php 
$mailkeeper = new Mailkeeper\SDK\Factory([
   'api-url'      => 'https://some.url',
   'api-key'      => 'sOmeKeY',
   'product-id'   => 777,
]);
```
