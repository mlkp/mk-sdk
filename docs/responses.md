# Responses

| Case    | HTTP status code | Response body                                                                          |
|---------|------------------|----------------------------------------------------------------------------------------|
| Success | 200              | ```{"message": "The request was successfully added to the queue.","data": ["..."]} ``` |

## [200] Success

_All methods return the validated data and success message._

```json
{
  "message": "The request was successfully added to the queue.",
  "data": [
    "..."
  ]
}
```

## [401] Authentication failed

_Wrong token or partner_id._

```json
{
  "message": "Authentication failed. Token is not active or not exist."
}
```

## [403] Forbidden

_API Token not install_

```
Authentication failed
```


## [422] Validation not passed

```json
{
  "{parameter_name}": [
    "The {parameter_name} field is required."
  ]
}
```

```json
{
  "{parameter_name}": [
    "The {parameter_name} must be a string."
  ]
}
```

```json
{
  "{parameter_name}": [
    "The {parameter_name} format is invalid."
  ]
}
```

## [500] Internal Server Error

We were unable to respond.
