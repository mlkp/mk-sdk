# Unsubscribe

```php
$client = Mailkeeper\SDK\Factory::create();
return $client->event()->unsubscribe($data);
```

## Validation

| Parameter           | Type             | Is Required | Description                                          |
|---------------------|------------------|-------------|------------------------------------------------------|
| product_id          | Unsigned Integer | Yes         | Unique product ID in the database                    |
| email               | String (255)     | Yes         | User email address                                   |
| unsubscribe_type_id | Unsigned Integer | Yes         | Will be provided by Mailkeeper manager               |
| domain_name         | String (255)     | No          | Domain name                                          |
| delivery_id         | String (255)     | No          | ID of an email sent to the customer's email address  |
| Custom Fields       | ...              | No          | Custom fields will be provided by Mailkeeper manager |
