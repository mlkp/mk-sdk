# Mailkeeper SDK Library Docs

## Documentation

- [Installation](install.md)
- [Examples](examples.md)
- [Responses](responses.md)

### Supported Methods
- [Registration  and send activation](register.md)
- [Registration without send activation](register-without-send.md)
- [Attributes update](attributes.md)
- [Unsubscribe](unsubscribe.md)
- [Event send](event.md)
