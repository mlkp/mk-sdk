# Attributes update

```php
$client = Mailkeeper\SDK\Factory::create();
$client->attributes()->update($data);
```

## Validation

| Parameter     | Type             | Is Required | Description                                          |
|---------------|------------------|-------------|------------------------------------------------------|
| product_id    | Unsigned Integer | Yes         | Unique product ID in the database                    |
| email         | String (255)     | Yes         | User email address                                   |
| Custom Fields | ...              | No          | Custom fields will be provided by Mailkeeper manager |
