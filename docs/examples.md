# Examples

> Parameters array depend on the settings of your product

### Example Without Factory

```php
<?php

require 'vendor/autoload.php';

use Mailkeeper\SDK\Http\Client;
use Mailkeeper\SDK\Resources\Register;

$client = new Client([
   'api-url'      => 'https://some.url',
   'api-key'      => 'sOmeKeY',
   'product-id'   => 777,
]);

$register = new Register($client);
$response = $register->register([
    'email' => 'user@test.com'
    'product_id' => 777,
]);
```


## Registration  and send activation

```php
$mailkeeper = Mailkeeper\SDK\Factory::create();
$mailkeeper->register()->register($data);
```


## Update Attributes

```php
$mailkeeper = Mailkeeper\SDK\Factory::create();
$mailkeeper->attributes()->update($data);
```


## Unsubscribe

```php
$client = Mailkeeper\SDK\Factory::create();
$client->event()->unsubscribe($data);
```


## Event Send

```php
$client = Mailkeeper\SDK\Factory::create();
$client->eventSend()->send($data);
```
