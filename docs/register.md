# Registration and send activation

```php
$mailKeeper = Factory::create();
$response = $mailKeeper->register()->register($data);

```

## Validation

| Parameter     | Type             | Is Required | Description                                                                                              |
|---------------|------------------|-------------|----------------------------------------------------------------------------------------------------------|
| product_id    | Unsigned Integer | Yes         | ID of the product (provided by Mailkeeper manager)                                                       |
| email         | String (255)     | Yes         | User email address                                                                                       |
| ip            | IP               | No          | User IP address                                                                                          |
| channel_name  | String (255)     | No          | Name of the website or application which customer used to create an account                              |
| opt_in_name   | String (255)     | No          | Name of the point where you got the email of the customer (e.g. registation_form, pop_up, landing, etc.) |
| source        | String (255)     | No          | Name of the source of email traffic (e.g. facebook_campaign, preland, etc.)                              |
| callback_url  | URL              | No          | URL for result of a request (whether it was a successful registation or not and why)                     |
| Custom Fields | ...              | No          | Custom fields will be provided by Mailkeeper manager                                                     |

### Callback

#### Possible errors

| Response                                    | Description                                                                         |
|---------------------------------------------|-------------------------------------------------------------------------------------|
| Not valid email                             | Invalid Email Address                                                               |
| Mx hosts not found                          | Invalid domain of the email adress                                                  |
| Mailbox does not exist                      | Email Address does not exist (only for Gmail)                                       |
| Limiter: product:x, isReactivation:x, esp:x | Reached the limit for registations for set of product/isReactivation/esp parameters |
| Email exist with active subscribe           | Email address is already in the database and has an active subscription             |
| Email unsub                                 | Email address is already in the database and is unsubscribed from emails            |
| Word in blacklist rule: x                   | Email address contains a blacklisted word                                           |
| Regexp in blacklist rule: x                 | Email address contains blacklisted regexp                                           |
| Domain in blacklist rule: x                 | Email address contains blacklisted domain                                           |
| Email in blacklist rule: x                  | Email address is blacklisted                                                        |
| Reached main limit                          | Reached limit for reactivations. Only for Reactivations.                            |
| Reached domain limit                        | Reached domain limit for reactivations. Only for Reactivations.                     |
| Fail. No domains available                  | No available domains are left. Only for Reactivations.                              |
| No matches to group                         | Failed matching to any reactivation groups. Only for Reactivations                  |
| No active group for domains                 | No available groups for reactivations. Only for Reactivations                       |

#### Success Response structure
``` json
{
  "status": "SUCCESS",
      "email": "eMaiL@mAiL.CoM",
      "normalized_email": "email@mail.com",
      "data": {
        "product_id": 1,
        "email_id": 1,
          "domain_id": 1,
          "domain_name": "test.com",
          "receiver_id": 1,
          "receiver_domain_id": 1,
          "send_activation": true,
          "is_reactivation": false,
          "timestamp": 1640995200
      }
}
```
