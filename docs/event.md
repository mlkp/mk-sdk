# Event send

```php
$client = Mailkeeper\SDK\Factory::create();
$client->eventSend()->send($data);
```

## Validation

| Parameter     | Type             | Is Required | Description                                                                          |
|---------------|------------------|-------------|--------------------------------------------------------------------------------------|
| product_id    | Unsigned Integer | Yes         | Unique product ID in the database                                                    |
| email         | String (255)     | Yes         | User email address                                                                   |
| event_type    | String (255)     | Yes         | Type of the event. Will be provided by Mailkeeper manager                            |
| domain_name   | String (255)     | No          | Domain name                                                                          |
| callback_url  | URL              | No          | URL for result of a request (whether it was a successful registation or not and why) |
| placeholders  | Array            | No          | *more details below                                                                  |
| Custom Fields | ...              | No          | Custom fields will be provided by Mailkeeper manager                                 |

*Placeholders consist two arrays - "text" and "links":
- Text is inserted into the email content in the same form as you sent it to us, without any
changes. In this array you need to put such data as username, age, links to user avatars or other pictures, etc.
- Links is changed on the Mailkeeper side, we add a redirect to the links to track the clicks on
them. This array should contain all the links the user should click on in the email.

Example of the "placeholders" array:

```json
"placeholders": 
{
    "text": 
    {
        "username": "John",
        "age":"22"
    },
    "links": 
    {
        "profile_link": "https://www.google.com/",
        "unsubscribe_link": "https://www.google.com/"
    }
}
```

### Callback

#### Possible errors
| Response                    | Description                                                 |
|-----------------------------|-------------------------------------------------------------|
| email_not_found             | Email not found in Mailkeeper database                      | 
 | receiver_not_found          | Email not found in Mailkeeper database of submitted product | 
 | event_type_not_found        | Invalid event_type parameter passed                         | 
 | receiver_locked             | Unable to send because of safety reasons                    | 
 | failed_rules_matching       | Unable to send, invalid segment                             | 
 | failed_general_restrictions | Unable to send because of safety reasons                    | 
 | failed_content_collector    | Unable to send, missing content                             |
