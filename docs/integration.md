# Integration

> Product - website that wishes to set up email marketing though Mailkeeper
> 
> Mailkeeper - email marketing platform

## Domain setup

From the side of the Product, it is necessary to do the following:

- Add DNS records for SPF, DKIM, and DMARC

- Set A-records to those IPs that we will provide

- Set CNAMEs for images and clicks-tracking subdomains

- Create mailboxes and configure incoming email forwarding

## Creating a product on the Mailkeeper side

From the Product side, it is necessary to compile a list of user attributes that will be transmitted and stored in the Mailkeeper that will subsequently allow for building mailing logic and analytics.

Examples:

- The time of the last login to the product attribute

- The presence of a premium subscription attribute

- The user's gender attribute

## API for user data and landing links

The Product needs to implement an API with all the relevant user data and landing links that will be transferred to Mailkeeper upon making a request call and subsequently used for sending mails. This is required for activation/welcome and filler emails, as these are initiated on the Mailkeeper's side.

## Registration request

Immediately after a user’s registration on the Product, Mailkeeper has to be informed through a registration call [Registration  and send activation](/docs/register.md). In this call you will need to send both the email and user data. Mailkeeper will register the email in the system and send an activation/welcome email.

## Unsubscribes from emails on the Product’s side.

It is essential that the Product provides an exhaustive list of possible scenarios when the user needs to be unsubscribed from the mailing list and sets up a call with the unsubscribe method to Mailkeeper [Unsubscribe](/docs/unsubscribe.md).

Examples:

- The user unsubscribed from the mailing list by changing the preferences/settings on the Product

- The user deleted his account on the Product

- The user's account was banned by the moderators

## Updating user’s attributes

There is a need to set up a separate call for each attribute from the user’s attribute list [Attributes update](/docs/attributes.md) and transfer the updates of these attributes to ensure that the information stored on Mailkeeper is always up-to-date.

Examples:

- The last login time attribute on the product - transfer the time of the last time the user visited the Product

- The premium subscription attribute - transfer when the user purchases or cancels the subscription

## Event Emails

Event emails notify the user of the events that are happening within the Product. These are usually emails about likes, messages, visits, mutuals, etc. These are also so-called system emails, notifying of password recovery, account ban, and premium purchases.

The Product needs to transfer all the events occurring on the Product’s side by setting up a call to the event method of Mailkeeper [Event send](/docs/event.md). Upon receiving the event, Mailkeeper will make the decision whether to send the mail or not based on the mailing logic.

<u>Example:</u>
A user on the Product received an incoming message from another user. The Product calls the Mailkeeper's event method and sends information that the message event has occurred and transfers all the necessary information for sending the mail.

Also, Product needs to provide requirements for the design of the letters - what should be used and what not.

## Filler Emails

Filler Emails are emails for users who have insufficient or no events for event emails. In other words, they are placeholders which will bring the user back to the product. These emails are about interesting features of the Product, tips on how to increase activity, how to use the Product, etc.
Product needs to provide requirements for the design of the letters - what should be used and what not.

## Transferring analytical data from Mailkeeper to the connected product.

Mailkeeper has the ability to pass analytical data on sends, deliveries, email reads, clicks on links, email confirmations and unsubscribes to the Product.
To do this, the Product must implement an API to pass data in realtime and save the data to its own database.
