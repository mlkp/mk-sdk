# Mailkeeper SDK Library Docs

## Documentation

- [Installation](/docs/install.md)
- [Examples](/docs/examples.md)
- [Responses](/docs/responses.md)
- [Integration](/docs/integration.md)

### Supported Methods
- [Registration  and send activation](/docs/register.md)
- [Attributes update](/docs/attributes.md)
- [Unsubscribe](/docs/unsubscribe.md)
- [Event send](/docs/event.md)
